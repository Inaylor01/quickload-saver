This App saves the custom genomes between IGB sessions.

To run the the App:

1. Install the App.
2. Click on File Menu.
3. Select **Save Custom Genome to Local Quickload Site**

Observe that a new dialog box opens for the user to select the directory in which the quickload site consisting of metadata files would be saved.
 
